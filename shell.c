#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/reg.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <string.h>
#include <time.h>
#include <sys/user.h>
#include <fcntl.h>
#include <stdlib.h>
#include <limits.h>


void logg();
void getdata();
time_t getSeconds();
int bruteForce();

struct timeval start;
struct timezone tzp;


int main()
{
		while(1)
		{
				char command[100];
				char comcopy[100];
				char *allCommands[50];
				char *paths[99999];

				int i = 1;
				int status;
				int f=0;
				int waiter;
				int x = 0;
				int spill = 0;
				int insyscall;

				printf("Enter a command: ");
				
				fgets(command, sizeof(command), stdin);
				// Command blir endret underveis, så lager en kopi av hele kommando-stringen her:
				strcpy(comcopy, command);
				
				//fgets() registrerer linjehopp. Fjerner det her:
				int last = strlen(command) - 1;
				command[last] = '\0';

				int last2 = strlen(comcopy) - 1;
				comcopy[last2] = '\0';
				
				//Skaper en string av de første karakterene frem til et mellomrom i "command"
				//og legger den i første index av adressetabellen "allCommands"
				allCommands[0] = strtok(command, " ");

				//Fyller opp indeksene i "allCommands" med argumentene videre fra "commands"
				while( (allCommands[i] = strtok(NULL, " ") ) )
						i++;

				while(allCommands[x] != '\0') 
				{
					if(strcmp(allCommands[x],"./spill") == 0)
						spill = 1;
					x++;
				}
				//Henter ut tiden 'før' systemkall
				time_t start;
				start = getSeconds();
				
				//Oppretter en barneprosess her
				int isChild = fork();

				//Dersom "isChild" er positiv (0), er vi i barneprosessen 
				if( isChild == 0)
				{
						//Ptrace begynner her å spore handlingene til prosessen
						ptrace(PTRACE_TRACEME, 0, NULL, NULL);
						//Kjører her kommandoene og argumentene fylt opp i allCommands-tabellen
						execvp(allCommands[0], allCommands);
						//Forsikrer at barneprosessen blir terminert dersom problemer skulle oppstå
						exit(1);
				}


				else
				{

						int j = 0;
						while(1 != 0)
						{
								char *path;
								//Foreldreprosessen venter her til barneprosessen er ferdig, 
								//og tar vare på returverdien fra wait som vil være pid'en til prosessen (Process ID)
								waiter = wait(&status);
								
								//WIFEXITED sjekker om barneprosessen ble terminert riktig, og bryter ut av løkka om den ikke ble det.
								if(WIFEXITED(status))
										break;
								
								//Ptrace finner her hvilket systemkallnummer som barneprosessen(exec) kjører, fra registeret %eax   
								long eax = ptrace(PTRACE_PEEKUSER, isChild, 4 * ORIG_EAX, NULL);

								//SYS_open er nummer 5
								if(eax == SYS_open)
								{
								//	if(insyscall == 0)
								//	{
										//Det må allokeres plass i minnet for "path".  
										path = malloc(512);

										//Henter ut ebx-registernummeret til "SYS_open", som vil peke på hva som ble åpnet
										long ebx = ptrace(PTRACE_PEEKUSER, isChild, 4 * EBX, NULL);
										
										//Henter ut path-stringen til filen som ble åpnet (ved SYS_open), og legger den i "path"
										getdata(isChild, ebx, path, 512);
										int o=0;

										//Passer på at "paths"-tabellen ikke inneholder den nåværende "path"-stringen fra før
										while(paths[o] != 0x0)
										{
												if (  (strcmp(path, paths[o])) == 0)
												{
														j--;
														break;
												}
												o++;
										}
										
										paths[j] = path;
										j++;
								//		insyscall = 1;
								//	}
								//	else
								//		insyscall = 0;
								}

								if(spill == 1)
								{
									if(eax == SYS_read)
									{
									//	printf("5 ");
										if(insyscall == 0)
										{	
											long ebx = ptrace(PTRACE_PEEKUSER, isChild, 4 * EBX, NULL);
											if(ebx == 0)
											{
												eax = ptrace(PTRACE_PEEKUSER, isChild, 4 * EAX, NULL);
												if(eax > 3)
												{
													printf("Not legit! ");
													kill(isChild, SIGTERM);
												}
												else
													printf("Legit ");
											}	
											insyscall = 1;
										}
										else
											insyscall = 0;
									}
								}
									insyscall = 0;

								//Stopper etter hvert systemkall:
								ptrace(PTRACE_SYSCALL, isChild, NULL, NULL);
								f++;
						}
				}

				logg(comcopy, start, paths, f, waiter);


		}
		return 0;
}

time_t getSeconds()
{
		struct timeval time; //Inneholder: tv_sec og tv_usec

		//Henter ut hvor mange sekunder siden Epoch(Jan. 01. 1970):
		gettimeofday(&time, NULL);
		return time.tv_sec;
}


void logg (char commands[100], time_t start, char ** paths, int f, int waitReturn)
{
		char tmp[20] = {0x0};
		time_t slutt;
		char startBuffer[50];
		char sluttBuffer[50];

		int output;
		int s = 0;
		
		//Må kopiere returverdien til "waitReturn" til char-tabellen "tmp". Gjør den altså til en string.
		//Den typiske løsningen fungerte ikke av en eller annen grunn. 
		sprintf(tmp, "%d", waitReturn);
			
		//Henter ut tiden etter systemkallet
		slutt = getSeconds();

		//Fyller opp bufrene med start- og sluttiden. localtime() regner ut det faktiske tidspunktet på dagen.
		//"%T" er flagg som formaterer tiden slik: HH:MM:SS
		strftime(startBuffer, 50,"%T" , localtime(&start));
		strftime(sluttBuffer, 50,"%T" , localtime(&slutt));

		//output er et tall på hvor i minnet filen "output.txt" ligger. 
		output = open("output.txt", O_RDWR | O_APPEND | O_CREAT, S_IRWXU );

		//Dersom det ble faktisk kjørt et legitimt systemkall vil ikke første indeks i "paths" være tom
		if (paths[s] != 0x0)
				write(output, "\nSuccessful Command: \n" ,22 );
		else
				write(output, "\nCommand: \n" ,11 );

		write(output, commands, strlen(commands) );
		//Tappert forsøk på pen formatering i loggfila:
		write(output, "\t\t\t\t\t\t", bruteForce(strlen(commands)) );
		write(output, "Start: " , 7 );
		write(output, startBuffer, strlen(startBuffer));
		write(output, "\t\tSlutt: ", 8);
		write(output, sluttBuffer, strlen(sluttBuffer));
		write(output, "\t\tReturn: ", 10);
		write(output, tmp, strlen(tmp));
		write(output, "\n", 1);

		//Skriver ut alle unike filer som ble åpnet av SYS_open til output-fila
		while ( paths[s] != 0x0)
		{
				write(output, paths[s], strlen(paths[s]) );
				write(output, "\n", 1);	
				s++;
		}
		//Tømmer adresse-tabellen slik at gammel informasjon ikke kan bli brukt videre:
		memset(paths, 0, sizeof(paths));

		close (output);
}

//Kode hentet fra [ http://www.linuxjournal.com/article/6100?page=0,2 ]
//Funksjonen finner størrelsen på det første argumentet til SYS_open
//og skriver inn path'en til filen som SYS_open åpner som en string til "path" (call by reference)
const int long_size = sizeof(long);
void getdata(pid_t child, long addr, char *str, int len)
{
		char *laddr;
		int i, j;
		//Lagrer forskjellige datatyper på samme sted i minnet.
		union u
		{
				long val;
				char chars[long_size];

		}data;

		i = 0;
		j = len / long_size;
		laddr = str;
		while(i < j)
		{
				data.val = ptrace(PTRACE_PEEKDATA,
								child, addr + i * 4,
								NULL);
				memcpy(laddr, data.chars, long_size);
				++i;
				laddr += long_size;
		}

		j = len % long_size;

		if(j != 0)
		{
				data.val = ptrace(PTRACE_PEEKDATA,
								child, addr + i * 4,
								NULL);
				memcpy(laddr, data.chars, j);
		}
		str[len] = '\0';
}

//Bruteforce måte å få penere formatering på
int bruteForce(int length)
{
		if( length < 4)
				return 6;
		else if( length < 8)
				return 5;
		else if( length < 12) 
				return 4;
		else if( length < 16)
				return 3;
		else 
				return 2;	
}
