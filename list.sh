#!/bin/bash

list=""
count=0
# Går igjennom hele output.txt fila, linje for linje
cat output.txt | while read LINE

do
	let count++
	#Bruker sed for å hente ut linje nr. "count" til "list"
	list=$(sed -n $count'p' output.txt)
	#Skriver ut linjen under "Successful Command" dersom den faktisk var "suksessfull"
	if [ "$list" = "Successful Command: " ]	
	then
			sed -n $((count+1))'p' output.txt
			sed -n $((count+2))'p' output.txt
			

	fi
done
